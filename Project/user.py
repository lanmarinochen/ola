import numpy as np
import statsmodels.api as sm
#Assumption: Product := Protein Powder with an average price of 30€/kg
class user:
    def __init__(self, user_class: str):
        '''
        C1 Professional && independent of gender => Price Sensitivity = low;  
        C2 Amateur && female => Price Sensitivity = medium; Less sensitive to fitness influencer content
        C3 Amateur && male => Price Sensitivity = high; More sensitive to fitness influencer content
        See Curves in documentation for more information
        Gender := True <=> Female
                  False <=> Male
        ''' 
        
        self.user_class = user_class
        if self.user_class == "C1":
            self.professional = True 
            self.historical_conversion = {15: 0.9, 20: 0.6, 25: 0.3, 30: 0.05, 35: 0.001} #Needs to be changed
            #Set gender with a uniorm distribution and a prob of 0.5 to either true or false upon intialization 
            self.gender = True if np.random.uniform(0, 1) > 0.5 else False

        elif self.user_class == "C2": 
            self.historical_conversion = {15: 0.85, 20: 0.78, 25: 0.75, 30: 0.15, 35: 0.01} #Needs to be changed
            self.gender = True
            self.professional = False

        elif self.user_class == "C3":
            self.historical_conversion = {15: 0.8, 20: 0.79, 25: 0.77, 30: 0.25, 35: 0.05} #Needs to be changed 
            self.gender = False
            self.professional = False

        self.conversion_rate_model = self.get_fitted_conversion_rate_model()

    def get_cost(self, bid: int): #Will be related to bid and clicks
        if self.user_class == "C1": 
            cost = 0.1 * bid #Placeholder 
        elif self.user_class == "C2":
            cost = 0.2 * bid #Placeholder
        elif self.user_class == "C3":
            cost = 0.3 * bid #Placeholder
        return cost
    
    def get_clicks(self, bid: int):
        # C1 
        if self.user_class == "C1":
            clicks = bid**2 + 8*bid + 10 #Placeholder
        # C2
        elif self.user_class == "C2":
            clicks = bid**2 + 6*bid + 8 #Placeholder
        # C3
        elif self.user_class == "C3":
            clicks = bid**2 + 4*bid + 6 #Placeholder
        # Adding Gaussian noise to the average curve
        clicks += np.random.normal(0, 1)
        return clicks

    def get_fitted_conversion_rate_model(self): 

        conversions: list = list(self.historical_conversion.values())
        prices: list = list(self.historical_conversion.keys())
        for i in range(len(prices)):
            print(f"Price: {prices[i]} Conversion: {conversions[i]}")
        print(f"Prices: {prices}")
        prices = sm.add_constant(prices)
        print(f"Changed Prices: {prices}")
        model = sm.Logit(conversions, prices) #Needs to be dependent on user class, see documentation e.g. C1 => Polynomial(cubic idk), C2 => Logit, C3 => Polynomial (Quadratic)
        results = model.fit()
        return results
    
    def get_conversion_rate(self, price):
        # Create a 2D NumPy array with shape (1, 2) from the scalar price value
        price_array = np.array([[1, price]])

        # Use the predict method of the model to get the conversion probability
        conversion_probability = self.conversion_rate_model.predict(sm.add_constant(price_array))

        return conversion_probability[0]
