import numpy as np
from user import user
class PricingOptimizer:
    def __init__(self, num_classes, bids, contexts):
        #num_classes & contexts is currently not used
        self.num_classes = num_classes
        self.bids = bids    
        self.contexts = contexts
    #@staticmethod
    def reward(self, clicks, conversion_prob, margin, cost):
        """
        The reward function that calculates the reward for a given set of inputs.
        """
        return clicks * conversion_prob * margin - cost

    def find_rewards(self, user, prices, margin):
        """
        Finds the best price for a given class of users.
        """
        rewards = []
        for bid in self.bids:
            clicks = user.get_clicks(bid)
            cost = user.get_cost(bid)
            for price in prices:
                conversion_prob = user.get_conversion_rate(price)
                print(f"conversion_prob: {conversion_prob}")
                print(f"clicks: {clicks}")
                print(f"margin: {margin}")
                print(f"cost: {cost}")
                r = self.reward(clicks=clicks, conversion_prob=conversion_prob, margin=margin, cost=cost)
                rewards.append([price, bid, r])
        #best_price = np.array(rewards)[np.argmax(rewards)][:2]
        return rewards
        #return best_price
    #@staticmethod
    def find_max_reward_and_bid_given_a_price(self, user, prices, picked_price, margin):
        reward = self.find_rewards(user.get_clicks(), user.get_conversion_rate(prices), margin, user.get_cost(prices))
        NPreward = np.array(reward)
        #Create a subset of the rewards where the price is the picked price
        subset = NPreward[NPreward[:, 0] == picked_price]
        #Find the index of the value with the maximum reward in the subset
        max_reward_index = np.argmax(subset[:, 2])
        #Return the bid and the reward
        return subset[max_reward_index][1], subset[max_reward_index][2]
        

    def find_maximum_achievalbe_reward(self, user, prices, margin):
        reward = self.reward(user.get_clicks(), user.get_conversion_rate(prices), margin, user.get_cost(prices))
        NPreward = np.array(reward)
        max_reward = NPreward[np.argmax(NPreward[:, 2])][2]
        return max_reward
        

    def find_maximum_reward(self, user, prices, margin):
        rewards = self.find_rewards(user, prices, margin)
        #Return the best uppermost achiebale reward
        RewardsNP = np.array(rewards)
        best_price = RewardsNP[np.argmax(RewardsNP[:, 2])][0]
        print(rewards)

    
    def find_best_price(self, user, prices, margin):
        rewards = self.find_rewards(user, prices, margin)
        #Return the best price
        RewardsNP = np.array(rewards)
        best_price = RewardsNP[np.argmax(RewardsNP[:, 2])][0]
        return best_price

    def find_best_bid(self, user, best_price, margin):
        """
        Finds the best bid for a given class of users and the best price.
        """
        rewards = []
        for bid in self.bids:
            clicks = user.get_clicks(bid)
            cost = user.get_cost(bid)
            conversion_prob = user.get_conversion_rate(best_price)
            r = self.reward(clicks, conversion_prob, margin, best_price - bid)
            rewards.append([bid, r])
            #get the best bid
        best_bid = np.array(rewards)[np.argmax(rewards)][:1]
        return best_bid
        #best_bid = self.bids[np.argmax(rewards)]
        #return best_bid

    def find_rewards_for_bids(self, user, best_price, margin):
        """
        Finds the best bid for a given class of users and the best price.
        """
        rewards = []
        for bid in self.bids:
            clicks = user.get_clicks(bid)
            cost = user.get_cost(bid)
            conversion_prob = user.get_conversion_rate(best_price)
            r = self.reward(clicks, conversion_prob, margin, best_price - bid)
            rewards.append([bid, r])
            #get the best bid
        #best_bid = np.array(rewards)[np.argmax(rewards)][:1]
        return rewards

    def optimize(self, class_data):
        """
        Optimizes the pricing for a given set of class data.
        """
        best_prices = []
        for i in range(self.num_classes):
            best_price = self.find_best_price(class_data[i])
            best_prices.append(best_price)

        best_bids = []
        for i in range(self.num_classes):
            best_bid = self.find_best_bid(class_data[i], best_prices[i])
            best_bids.append(best_bid)

        return {'best_prices': best_prices, 'best_bids': best_bids}
    


'''
class Users:
    def __init__(self):
      self.class_data = [
          {
              'clicks': 100,
              'conversion_prob': 0.1,
              'margin': 10,
              'cost': 50
          },
          {
              'clicks': 200,
              'conversion_prob': 0.05,
              'margin': 20,
              'cost': 100
          },
          {
              'clicks': 300,
              'conversion_prob': 0.15,
              'margin': 30,
              'cost': 150
          }
      ]
'''


""" users = Users()
pricing_optimizer = PricingOptimizer(num_classes=3, num_bids=100, contexts=[1,2])
results = pricing_optimizer.optimize(class_data=users.class_data)

print('Best prices:', results['best_prices'])
print('Best bids:', results['best_bids'])
 """