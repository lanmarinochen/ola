import numpy as np

class Learner():
    def __init__(self, n_arms):
        self.n_arms = n_arms
        self.t = 0
        self.rewards_per_arm = x = [[] for arm in range(n_arms)] # list of lists depending of arms and number of times we pull a given arm
        self.collected_rewards = np.array([]) # array of rewards collected at each round
    
    def update_observations(self, pulled_arm, reward):
        self.t += 1
        self.rewards_per_arm[pulled_arm].append(reward) #recall: python list 
        self.collected_rewards = np.append(self.collected_rewards, reward) #recall: numpy array

    
    