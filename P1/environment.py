import numpy as np
class Environment():
    
    def __init__(self, n_arms, probabilities):
        #Constructor
        self.n_arms = n_arms
        self.probabilities = probabilities
        
    def round(self, pulled_arm):
        #Returns the reward of the pulled arm
        #Binomially distributed with p <=> of pulled arm
        reward = np.random.binomial(1, self.probabilities[pulled_arm])
        return reward